#!/usr/bin/python
# -*-coding:utf-8-*-

import sys
import MeCab

__author__ = 'bibreen@gmail.com'

POS_ID_UNKNOWN = 0
POS_ID_NOUN = 150

unacceptable_connections = {
    'EC/ETM', 'EC/NN',
    'JKS/NN', 'JKC/NN', 'JKG/NN', 'JKO/NN', 'JKB/NN', 'JKV/NN', 'JKQ/NN',
    'JX/NN', 'JC/NN',
}


def main():
    if len(sys.argv) < 2:
        sys.stderr.write('usage: ' + sys.argv[0] + ' <file name>')
        exit(1)

    tagger = MeCab.Tagger('-d /usr/local/lib/mecab/dic/mecab-ko-dic/')

    file_name = sys.argv[1]
    with open(file_name, mode='r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            line = line.strip()
            if not is_valid_sentence(line):
                continue
            find_unregistered_morpheme(tagger, line)


def is_valid_sentence(sentence):
    toks = sentence.split(' ')
    for tok in toks:
        tok_len = len(tok.decode('utf-8'))
        if tok_len > 5:
            return False
    return True


def find_unregistered_morpheme(tagger, sentence):
    node = tagger.parseToNode(sentence)
    eojeols = split_by_whitespace(node)
    for eojeol in eojeols:
        if is_strange_eojeol(eojeol):
            print(get_eojeol_string(eojeol) + ' -- ' + sentence)


def split_by_whitespace(node):
    output = []
    eojeol = []

    while node:
        if node.stat is MeCab.MECAB_BOS_NODE:
            node = node.next
            continue
        if node.stat is MeCab.MECAB_EOS_NODE:
            if eojeol:
                output.append(eojeol)
                break
        if has_whitespace(node) or is_symbol(node):
            if eojeol:
                output.append(eojeol)
            eojeol = []

        if not is_symbol(node):
            eojeol.append(node)
        node = node.next
    return output


def has_whitespace(node):
    return node.rlength - node.length > 0


def is_symbol(node):
    pos = get_pos(node)
    return pos == 'SY' or pos == 'SSO' or pos == 'SSC'


def get_features(node):
    return node.feature.split(',')


def get_pos(node):
    return get_features(node)[0]


def is_strange_eojeol(eojeol):
    if has_unknown(eojeol):
        return True
    if has_unacceptable_connection(eojeol):
        return True
    if has_too_many_nouns(eojeol):
        return True
    return False


def has_unknown(nodes):
    for node in nodes:
        if node.posid == POS_ID_UNKNOWN:
            return True
    return False


def has_unacceptable_connection(nodes):
    for node in nodes[0:-1]:
        if is_unacceptable_connection(node, node.next):
            return True
    return False


def is_unacceptable_connection(left_node, right_node):
    global unacceptable_connections

    if not right_node:
        return False

    left_pos = get_pos(left_node)
    right_pos = get_pos(right_node)
    k = left_pos + '/' + right_pos
    if k in unacceptable_connections:
        return True
    else:
        return False


def has_too_many_nouns(nodes):
    count = 0
    for node in nodes:
        if node.posid == POS_ID_NOUN:
            ++count
    if count > 2:
        return True
    return False


def get_eojeol_string(eojeol):
    output = []
    for node in eojeol:
        output.append(node.surface + '/' + get_pos(node))
    return '+'.join(output)


def print_eojeols(eojeols):
    for eojeol in eojeols:
        for node in eojeol:
            print(node.surface + ',' + get_pos(node))
        print('----------')


if __name__ == '__main__':
    main()